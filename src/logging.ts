import chalk from "chalk"

const { dim, red, green, yellow, bold } = chalk;
const formatter = new Intl.DateTimeFormat(undefined, { dateStyle: 'short', timeStyle: 'short' });

function log(...params: any[]) {
    console.log(dim(`[Scrapper] - ${process.pid} - ${formatter.format(Date.now())} -`), ...params);
}

export function warnLog(message: string) {
    log(yellow(`${bold('WARN')} - ${message}`));
}

export function infoLog(message: string) {
    log(green(bold('INFO') + ' - ' + message));
}

export function errorLog(message: string | Error, stack?: string) {
    log(red(bold('ERROR') + ' - ' + (typeof message == 'string' ? message : message.message)));
    if (stack)
        log(red(bold('ERROR') + ' - ' + stack));
    else if (typeof message != 'string' && message.stack) {
        log(red(bold('ERROR') + ' - ' + message.stack));
    }
}