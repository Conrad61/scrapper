import { from, mergeMap } from 'rxjs';
import './scrapper';
import scrapeSite from './scrapper';

const targets = ['https://google.com', 'https://x.com', 'https://fb.com'];
from(targets).pipe(
    mergeMap(target => scrapeSite(target))
).subscribe({
    next: x => console.log(x),
    error: (error: Error) => {
        console.log(error.name);
        console.log(error.message);
    }
});