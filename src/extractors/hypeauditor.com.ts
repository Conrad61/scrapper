import { Page } from "puppeteer";
import { concatMap, forkJoin, from, identity, map, mergeMap, of, tap } from "rxjs";
import { Influencer } from "../models";

export default (page: Page) => {
    return from(page.$$eval('.report-list__item', items => items.map(item => ({
        socialHandle: item.querySelector('.report-card-content-top-left__social').innerText
    }) as Influencer))).pipe(
        // concatMap(identity),
        // mergeMap(handle => {
        //     console.log(handle.frame.content());
        //     return forkJoin([
        //         handle.$eval('.report-card-content-top-left__social', element => element.innerText),
        //         handle.$eval('.report-card-content-top-left__fullName', element => element.innerText as string),
        //         handle.$eval('.report-card-content-top-left__username', element => element.innerText as string),
        //         of('dfd'), // handle.$eval('img', img => img.src as string),
        //         of('dfd'), // handle.$eval('img', img => img.src as string),
        //         // handle.$eval('.report-card-content-bottom > .report-card-content-bottom-content > .report-card-content-bottom-content__value', element => element.innerText)
        //     ]);
        // }),
        // map(([socialPlatform, fullNames, handle, avatar, followerCount]) => {
        //     console.count('hello');
        //     const url = new URL(page.url());
        //     return {
        //         source: url.origin,
        //         avatar: avatar,
        //         followers: followerCount,
        //         name: fullNames,
        //         socialPlatform,
        //         socialHandle: handle
        //     } as Influencer;
        // })
    );
}