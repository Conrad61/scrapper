import { Page } from "puppeteer";
import { Observable } from "rxjs";
import { Influencer } from "../models";

export type ExtractorFn = (page: Page) => Observable<Influencer>;