import { HTTPResponse, Page } from "puppeteer";
import { EMPTY, Observable, catchError, concatMap, from, map, mergeMap, throwError } from "rxjs";
import { Influencer } from "../models";
import { warnLog } from "../logging";
import { ExtractorFn } from "../extractors";

function getExtractor(response: HTTPResponse): Observable<ExtractorFn> {
    const url = new URL(response.url());
    return from(import(`../extractors/${url.hostname}`)).pipe(
        map(module => module.default as (page: Page) => Observable<Influencer>),
        catchError((error: Error) => {
            if (error.name === 'ResolveMessage') {
                warnLog(`Extractor not found for "${url.hostname}"`);
                return EMPTY;
            }
            return throwError(() => error);
        })
    );
}

export default function scrape(page: Page) {
    return from(page.goto('https://www.google.com/search?q=top+facebook+influencers')).pipe(
        mergeMap(response => {
            if (response) {
                return page.$$eval('a[data-sxg-url]', links => links.map(link => link.href)) as Promise<string[]>;
            }
            return EMPTY;
        }),
        mergeMap(urls => {
            return from(urls).pipe(
                concatMap(url => page.goto(url)),
                mergeMap(response => {
                    if (response && response.ok())
                        return getExtractor(response);
                    return EMPTY;
                }),
                mergeMap(fn => fn(page))
            )
        }),
        catchError((error: Error, caught) => {
            return throwError(() => error);
        })
    );
}