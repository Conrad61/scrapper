import { Browser, launch, Page } from "puppeteer";
import { EMPTY, from, fromEvent, of, Subject, switchMap, take, tap } from 'rxjs';
import { errorLog, infoLog, warnLog } from "./logging";
import { Influencer } from "./models";

let browser: Browser;
const browserReady$ = new Subject<void>();
const registry = {
    './scrapper-impl/facebook-scrapper': ['facebook.com', 'fb.com']
}

async function determineScrapperImplementation(url: string): Promise<((page: Page) => Promise<Influencer | Influencer[]>) | undefined> {
    const parsedUrl = new URL(url);
    const importUrl = Object.entries(registry).find(([_, aliases]) => aliases.includes(parsedUrl.hostname));
    if (!importUrl) return undefined;
    return await import(importUrl[0]).then(module => module.default);
}

export default function scrapeSite(url: string) {
    const src$ = from(determineScrapperImplementation(url)).pipe(
        tap(() => infoLog(`Preparing site: ${url} for scrapping. Looking up scrapper implementations...`)),
        switchMap(fn => {
            if (!fn) {
                warnLog(`The site ${url} has no scrapper implementations`);
                return EMPTY;
            }

            infoLog(`Scrapper implementation for ${url} found. Scrapping in progress...`);
            return from(browser.newPage()).pipe(
                switchMap(page => fn(page)),
                switchMap(data => Array.isArray(data) ? data : of(data))
            )
        }),
    );

    if (browser)
        return src$;
    return browserReady$.pipe(
        switchMap(() => src$)
    )
}


infoLog('Instantiating browser...');
from(launch({
    headless: 'new',
    defaultViewport: { width: 1920, height: 1080 }
})).subscribe({
    error: (error: Error) => {
        errorLog(`Error while starting browser instance - ${error.message}`);
        process.exit(1);
    },
    next: (instance) => {
        browser = instance;
        browserReady$.next();
        browserReady$.complete();
        // process.on('SIGINT')
        fromEvent(process, 'exit').pipe(
            take(1)
        ).subscribe(() => {
            infoLog('SIGINT detected - Closing browser instance');
            instance.close();
        });
    },
    complete: () => infoLog('Browser instantiated')
});