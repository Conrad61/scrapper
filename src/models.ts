export type Influencer = Partial<{
    name: string;
    socialHandle: string;
    socialPlatform: string;
    source: string;
    socialUrl: string;
    avatar: string;
    followers: string;
    following: string;
    extras: Record<string, any>;
}>;